package TestHomeWork;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    @BeforeTest
    public void beforeTest(){
        System.out.println();
        System.out.println("Старт ");
        System.out.println();
    }

    @AfterTest
    public void afterTest(){
        System.out.println();
        System.out.println("Стоп");
    }
}
