package HomeworkClass;

public class RoundClass {

    public RoundClass(int radius){
    this.radius = radius;
        System.out.println();
        System.out.println("Задан радиус : " + radius);
        System.out.println();
    }
    int radius;

    public double getArea(int radius){
        double area = Math.PI * radius * radius;
        System.out.println( "Площадь круга : " + area);
        return area;
    }

    public double getLength(int radius){
        double length = 2 * Math.PI * radius;
        System.out.println("Длина круга : " + length);
        return length;
    }
}
