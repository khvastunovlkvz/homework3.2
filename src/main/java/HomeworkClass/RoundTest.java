package HomeworkClass;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RoundTest extends BaseTest {

/*
    public static void main(String[] args) {

        RoundClass round1 = new RoundClass(10);

        round1.getArea(round1.radius);

        round1.getLength(round1.radius);
    }
*/

        RoundClass round2 = new RoundClass(1);

        @Test
        public void checkArea(){
        double expectedResult = Math.PI * round2.radius * round2.radius;
        double actualResult = round2.getArea(round2.radius);

            Assert.assertEquals(expectedResult, actualResult , "Тест провален");
        }

        @Test
        public void checkLength(){
        double expectedResult = 2 * Math.PI * round2.radius;
        double actualResult = round2.getLength(round2.radius);

            Assert.assertEquals(expectedResult, actualResult , "Тест провален");
        }



    }

